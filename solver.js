const encodedWord = "wordHere"

let decodedWord = ""
const alphabet = "abcdefghijklmnopqrstuvwxyz"

for(let i = 0; i < encodedWord.length; i++) {
    letterPlacement = findLetterPosition(encodedWord[i])

    let index = i
    if (index >= 6) {
        index = index % 6
    }
    
    switch (index) {
        case 0:
            decodedWord += decoder(letterPlacement, findLetterPosition("w"))
            break
        case 1:
            decodedWord += decoder(letterPlacement, findLetterPosition("o"))
            break
        case 2:
            decodedWord += decoder(letterPlacement, findLetterPosition("r"))
            break
        case 3:
            decodedWord += decoder(letterPlacement, findLetterPosition("d"))
            break
        case 4:
            decodedWord += decoder(letterPlacement, findLetterPosition("l"))
            break
        case 5:
            decodedWord += decoder(letterPlacement, findLetterPosition("e"))
            break        
    }
}

console.log(decodedWord)

function decoder(letter, code) {
    let decoded = letter - code

    if (decoded  < 0) {
        decoded = decoded + 26
    }

    return findLetterByPosition(decoded)
}

function findLetterPosition(letter) {
    return alphabet.indexOf(letter)
}

function findLetterByPosition(index) {
    return alphabet[index]
}
